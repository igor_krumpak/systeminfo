Router.route('/', function() {
    this.render('SysInfo', {
        data: function() {
            return SysInfos.find({}, {
                sort: {
                    time: -1
                },
                limit: 1
            }).fetch()[0];
        }
    });
});
