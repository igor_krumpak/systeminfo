var shell = Meteor.npmRequire('shelljs');
Fiber = Meteor.npmRequire('fibers');
SysInfos = new Mongo.Collection("sysinfos");

setInterval(function() {
    parseSysInfo();
}, 10000);

function parseSysInfo() {
    var sysInfo = {};
    sysInfo.cpu = parseInt(shell.exec("top -b -n2 | grep \"Cpu(s)\"|tail -n 1 | awk '{print $2 + $4}'").output.trim());
    var memory = shell.exec("top -b -n2 | grep \"KiB Mem\"|tail -n 1 | awk '{print $5, $7}'").output.trim().split(" ")
    sysInfo.f_ram = parseInt(memory[1]);
    sysInfo.u_ram = parseInt(memory[0]);
    sysInfo.time = new Date();
    console.log(sysInfo);
    Fiber(function() {
        Meteor.call("addSysInfo", sysInfo);
    }).run();
}
